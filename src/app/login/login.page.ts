import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import{
  FormGroup,
  FormControl,
  Validators,
  FormBuilder

}from '@angular/forms'
import {alertController} from '@ionic/core';
import { HttpClient, HttpHeaders, HttpClientModule} from '@angular/common/http';
import { AlertController, NavController,LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

formularioLogin: FormGroup;
  constructor(public fb: FormBuilder,
    public navCtrl:NavController,
    public alertCtrl: AlertController,
    public loadingController: LoadingController,
    public http: HttpClient) {
   this.formularioLogin= this.fb.group({
     'nombre': new FormControl("",Validators.required),
     'password': new FormControl("",Validators.required),
   })
   }

  ngOnInit() {
  }

  async ingresar() {
    var f = this.formularioLogin.value;
    
let loading = await this.loadingController.create({
message: 'Validando...'
});

//mostramos el loading para que se bloquee la pantalla
//y se muestre el mensaje de cargando
await loading.present();



//Se agrega  los encabezados para que se mande en formato json, 
//por que nuestro api solocita estos datos en formato json
const headers = new HttpHeaders();
headers.append("Accept", 'application/json');
headers.append('Content-Type', 'application/json');
const options = { headers: headers, withCredintials: false };

//Se crea el objeto en formato json
let postData = {
"nombre": f.nombre,
"contraseña": f.password
}
//Se realiza la peticion a la api
const data = await this.http.post("http://127.0.0.1/huellitas_api/login.php", postData, options).toPromise();

//se oculta el loading de cargando 
await loading.dismiss();

//se valida se el success que devuelve nuestro api es verdadero
//si es verdadero se direcciona al inicio
//si es falso se muestra un alert informandole
if(data['success']){
this.navCtrl.navigateRoot('home');
} else {
const alert = await this.alertCtrl.create({
  header: 'Información',
  message: data['msg'],
  buttons: ['Aceptar']
});

//se presenta el alert para informarle al usuario que fallo su ingreso
await alert.present();
}


}
}

