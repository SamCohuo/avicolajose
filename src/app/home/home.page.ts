import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule} from '@angular/common/http';
import { LoadingController, AlertController} from '@ionic/angular';
import{
  FormGroup,
  FormControl,
  Validators,
  FormBuilder

}from '@angular/forms'

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  formularioLogin: FormGroup;
  constructor( 
    public http: HttpClient,
    public alertController: AlertController,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.getEmpleados();
  }
  empleados: any[] = [];
    
    async getEmpleados() {
      let loading = await this.loadingController.create({
        message: 'Cargando Datos...'
      });
  
    //mostramos el loading para que se bloquee la pantalla
    //y se muestre el mensaje de cargando
      await loading.present();

  
      //Se agrega  los encabezados para que se mande en formato json, 
      //por que nuestro api solocita estos datos en formato json
      const headers = new HttpHeaders();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      const options = { headers: headers, withCredintials: false };
      
      //Se realiza la peticion a la api
      const data = await this.http.get("http://127.0.0.1/huellitas_api/empleados.php", options).toPromise();
      this.empleados = data['result'];
      //se oculta el loading de cargando 
      await loading.dismiss();
    }
  
  }

