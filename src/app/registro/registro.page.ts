import { Component, OnInit } from '@angular/core';
import{
  FormGroup,
  FormControl,
  Validators,
  FormBuilder

}from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';
import { from } from 'rxjs';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  formularioRegistro: FormGroup;
  constructor(public fb: FormBuilder,
    public alertCtrl:AlertController,
    public navCtrl:NavController
    ) {
    this.formularioRegistro= this.fb.group({
      'nombre': new FormControl("",Validators.required),
      'clave': new FormControl("",Validators.required),
      'contrasenia': new FormControl("",Validators.required),
      'confirmarContrasenia': new FormControl("",Validators.required)
    })
  }

  ngOnInit() {
  }
  async guardar(){
   // console.log('Dato Guardado');
    if(this.formularioRegistro.invalid){
      

    const alert =await this.alertCtrl.create({
      cssClass:'my-custom-class',
      header:'Campos requeridos',
      message:'Favor de ingresar todos los datos',
      buttons:['Aceptar']
    });
    await alert.present();
    return;
   }
   var f=this.formularioRegistro.value;
var usuario={
  nombre: f.nombre,
  contrasenia: f.contrasenia
}
localStorage.setItem('usuario',JSON.stringify(usuario));
this.navCtrl.navigateRoot('login');
  }
}

